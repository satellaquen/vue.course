import Vue from "vue";
import Vuelidate from "vuelidate";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import dateFilter from "@/filters/date.filter";
import messagePlugin from "@/utils/message.plugin";
import "./registerServiceWorker";
import "materialize-css/dist/js/materialize.min";

import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";

Vue.config.productionTip = false;

Vue.use(Vuelidate);
Vue.use(messagePlugin);
Vue.filter("date", dateFilter);

const firebaseConfig = {
  apiKey: "AIzaSyDQcC9vN5Q2aC64HBGsO15LlwdyGKreXak",
  authDomain: "vue-course-crm.firebaseapp.com",
  databaseURL: "https://vue-course-crm.firebaseio.com",
  projectId: "vue-course-crm",
  storageBucket: "vue-course-crm.appspot.com",
  messagingSenderId: "316031746875",
  appId: "1:316031746875:web:9249c9cb6b374ed39e6d1e"
};
firebase.initializeApp(firebaseConfig);

let app;

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount("#app");
  }
});
